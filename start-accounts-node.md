# Local machine node configuration (Accounts node)

```bash
export CELO_IMAGE=us.gcr.io/celo-testnet/celo-node:baklava
export NETWORK_ID=62320
```

```bash
mkdir celo-accounts-node
cd celo-accounts-node

# On your local machine
docker run \
    -v $PWD:/root/.celo \
    --rm -it $CELO_IMAGE init /celo/genesis.json

export BOOTNODE_ENODES=`docker run --rm --entrypoint cat $CELO_IMAGE /celo/bootnodes`
```

Run the node:
```bash
# On your local machine
docker run \
    -d --name celo-accounts \
    -p 127.0.0.1:8545:8545 \
    -v $PWD:/root/.celo \
    $CELO_IMAGE \
    --verbosity 3 \
    --networkid $NETWORK_ID \
    --syncmode full \
    --rpc \
    --rpcaddr 0.0.0.0 \
    --rpcapi eth,net,web3,debug,admin,personal \
    --bootnodes $BOOTNODE_ENODES \
    --allow-insecure-unlock
```

```bash
# On your local machine
export PROXY_EXTERNAL_IP="" #<PROXY-MACHINE-EXTERNAL-IP-ADDRESS>
```
