# Proxy node configuration

```bash
# On the proxy machine
mkdir celo-proxy-node
cd celo-proxy-node
```

```bash
export CELO_VALIDATOR_SIGNER_ADDRESS=""
docker run -v $PWD:/root/.celo --rm -it $CELO_IMAGE init /celo/genesis.json
export BOOTNODE_ENODES="$(docker run --rm --entrypoint cat $CELO_IMAGE /celo/bootnodes)"
```

```bash
# First, we create a new account for the proxy
nano .password

docker run \
    --name celo-proxy-password \
    -it --rm  -v $PWD:/root/.celo \
    $CELO_IMAGE account new --password /root/.celo/.password
```

```bash

export PROXY_ADDRESS=""
export VALIDATOR_NAME="blockforce.in"

docker run \
    --name celo-proxy -it --restart unless-stopped \
    -p 30303:30303 \
    -p 30303:30303/udp \
    -p 30503:30503 \
    -p 30503:30503/udp \
    -v $PWD:/root/.celo \
    $CELO_IMAGE \
    --verbosity 3 \
    --networkid $NETWORK_ID \
    --nousb \
    --syncmode full \
    --proxy.proxy --proxy.proxiedvalidatoraddress $CELO_VALIDATOR_SIGNER_ADDRESS \
    --proxy.internalendpoint :30503 \
    --etherbase $PROXY_ADDRESS \
    --unlock $PROXY_ADDRESS \
    --password /root/.celo/.password \
    --allow-insecure-unlock \
    --bootnodes $BOOTNODE_ENODES \
    --ethstats=$VALIDATOR_NAME@baklava-celostats-server.celo-testnet.org
```

```bash
# On the proxy machine, retrieve the proxy enode
export PROXY_ENODE="$(docker exec celo-proxy geth --exec "admin.nodeInfo['enode'].split('//')[1].split('@')[0]" attach | tr -d '"')"
```

Get IP address of the proxy if you don't know:

```bash
# On the proxy machine
dig +short myip.opendns.com @resolver1.opendns.com
```