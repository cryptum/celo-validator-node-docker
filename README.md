# Celo nodes testnet (baklava)

Create 3 VMs:
- Validator
- Proxy
- Attestation node (optional)

Set up environment variables as you follow the instructions:
```
source env.sh
```

### Docs:

- https://docs.celo.org/getting-started/baklava-testnet/running-a-validator-in-baklava
- https://medium.com/keyko/enter-the-d%C5%8Dj%C5%8D-celo-validator-set-up-d7ca49864dc7
